# Introduction

# Installations
## Home Assistant
This roughly follows the instructions located [here on the official home-assistant page](https://www.home-assistant.io/docs/installation/raspberry-pi/). Their instructions use virtualenv which I think is a bit overkill and harder to use within a service especially since we are putting this on its own user

1. Create a user `useradd -s /bin/bash -m -G dialout,gpio homeassistant`
2. Make sure that Python 3 is installed `apt -y install python3.5 python3-pip`
3. Change into the user `su - homeassistant`
4. Install the dependencies `python3 -m pip install --user homeassistant`

Now this will install Home Assistant to the user's Python library which is in `~/.local`.

5. Kick off Home Assistant manually first `~/.local/bin/hass`

This should take quite a while to run, it took my pi around 5min and crashed once. Just restart it and wait for it to succeed. Once it is finally done initializing, you can kill it and then install the service.

### Install service
1. Take the `homeassistant.service` and put it in `/etc/systemd/system/homeassistant.service`
2. Issue a `systemctl daemon-reload`
3. Start and enable the service `systemctl enable homeassistant; systemctl start homeassistant`
