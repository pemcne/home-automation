# Introduction
This will get you a openvpn installation complete with two factor authentication via google authenticator.

# Installation
1. Install openvpn `apt -y install openvpn`

## Generate keys
### Prepare the environment
1. Navigate into the easy-rsa directory `cd /usr/share/easy-rsa`
2. Modify vars
```
  export KEY_COUNTRY="US"
  export KEY_PROVINCE="State"
  export KEY_CITY="City"
  export KEY_ORG="None"
  export KEY_EMAIL=<email>
  export KEY_OU="None"
```
3. Symlink the openssl configuration `ln -s /etc/ssl/openssl.cnf /usr/share/easy-rsa/openssl.cnf`
4. Source the vars file `source ./vars`

### Building the keys
1. Generate the CA `./build-ca`
2. Generate the dh file `./build-dh`
3. Generate the server certificate `./build-key-server <servername>`
4. For every client you have, generate a certificate with a password `./build-key-pass <client>` or without a password `./build-key <client>`
5. Generate the tls key `openvpn --genkey --secret ta.key`

### Copy the keys
I prefer to keep all the necessary keys within the openvpn configuration folder

1. Make a keys directory `mkdir -p /etc/openvpn/server/keys`
2. Copy all of the necessary files from `/usr/share/easy-rsa/keys` to the new folder `cp ca.crt dh2048.pem <server>.crt <server>.key ta.key /etc/openvpn/server/keys/`

# Server tweaks
If you want your vpn clients to access the rest of your LAN, you'll need to do these tweaks.

## Set forward IP on the server
1. Edit the `/etc/sysctl.conf` file
2. Change `net.ipv4.ip_forward = 1`

## Enable the service
1. Transfer the service file provided to `/etc/systemd/system/openvpn-server.service`
2. Reload systemd `systemctl daemon-reload`
3. Enable and start the service `systemctl enable openvpn-server && systemctl start openvpn-server`

# Two factor auth
If you want to use two factor authentication for your vpn, you can do these steps. The plugin configuraiton line is already in the `server.conf` so remove that line if you do not want to use two factor authentication.
## Google Authenticator
1. Install the pam module `apt -y install libpam-google-authenticator`
2. Run the authenticator to generate the key `google-authenticator --time-based --disallow-reuse --force --rate-limit=3 --rate-time=30 --window-size=17 --issuer=none --label=<user@hostname> --secret=~/.user.google_authenticator > ~/user.auth`

## OTP Plugin
1. Clone down the repo `git clone https://github.com/evgeny-gridasov/openvpn-otp.git`
2. Install deps `apt -y install autoconf libtool libssl1.0-dev`
3. Configure and make install `./autogen.sh && ./configure --prefix=/usr && make install`

## Configure the OTP plugin
Just like the readme on the OTP plugin github page, you can take the key from the google authenticator run and use that to generate a configuration file for the plugin.

1. Get the key from the `~/user.auth`, it should be on the line `Your new secret key is: <keyhere>`
2. Create a file at `/etc/openvpn/server/otp`
3. Create as many users with as many authenticators as you want. The format for each line is `<USERNAME> otp totp:sha1:base32:<SECRETKEY>:<PASSCODE>:xxx *`

If you specify a passcode, Openvpn will only prompt for a single password when connecting to the server. You will just append the two factor code to your passcode as the password to send to the server when connecting. For example, when authenticating as user "bob" with the password "foobar", send the password of "foobar123456" to the server assuming the two factor code is 123456. 
