# Wireguard

## Requirements
I'm assuming you are running latest Raspbian

## Installation
1. Install dependencies `apt -y install libmnl-dev build-essential raspberrypi-kernel-headers`
2. Find the link to the latest release. Go [here](https://git.zx2c4.com/WireGuard/) and find the latest tag. Find the download link inside the tag.
3. Pull down the source `wget <link to tag tar.xz>`
4. Extract source `tar -Jxvf WireGuard-<version>.tar.xz`
5. Compile `cd WireGuard-<version>/src; make`
6. And install `sudo make install`
